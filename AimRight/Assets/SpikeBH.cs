﻿using UnityEngine;
using System.Collections;

public class SpikeBH : MonoBehaviour {
	bool StopChekingCollision;
	public bool effect;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(effect){
			GameObject.Find("Main Camera").transform.position = Vector3.Lerp(GameObject.Find("Main Camera").transform.position,new Vector3(0,-1.5f,-10),60*Time.deltaTime);
			GameObject.Find("Main Camera").GetComponent<Camera>().orthographicSize = Mathf.Lerp(GameObject.Find("Main Camera").GetComponent<Camera>().orthographicSize,3.3f,40*Time.deltaTime);
			GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = Color.Lerp(GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor,Color.red,40*Time.deltaTime);
			Invoke("StopEffect",0.06f);
		}
	
	}
	void StopEffect(){
		Destroy(gameObject);
		effect = false;
		GameObject.Find("Main Camera").GetComponent<PlayersBH>().ResetView=true;
		CancelInvoke("StopEffect");
	}
	void OnCollisionEnter2D(Collision2D coll){
		if(!StopChekingCollision){
			if(coll.gameObject.tag == "World"){
				gameObject.transform.position += new Vector3(0,Random.Range(0.0f,1.0f),0);
				GameObject.Find("Main Camera").GetComponent<PlayersBH>().NextTurn=true;
				GameObject.Find("Main Camera").GetComponent<PlayersBH>().InstantiateSpike();
			}
			else{
				Time.timeScale= 0.02f;
				effect = true;
				GetComponent<Collider2D>().enabled= false;
				GameObject.Find("Main Camera").GetComponent<PlayersBH>().CurrPlayerEliminated();
			}
			gameObject.transform.parent = coll.transform;
			Destroy(this.GetComponent<Rigidbody2D>());
			StopChekingCollision=true;
		}		
	}
}
