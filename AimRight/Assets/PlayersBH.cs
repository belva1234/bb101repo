﻿using UnityEngine;
using System.Collections;

public class PlayersBH : MonoBehaviour {
	int playersC = 0;
	public int currplayer=1;//always start first
	public GameObject[] PlayersFlag;
	public GameObject Source_Spike;
	GameObject LastSpike;
	[HideInInspector]
	public bool NextTurn=true;
	[HideInInspector]
	public bool ResetView;
	// Use this for initialization
	void Start () {
		playersC = GameObject.Find("GameInf").GetComponent<GameInfo>().players;
		PlayersFlag[0].SetActive(true);
		InstantiateSpike();
		for(int i=playersC;i<4;i++){
			PlayersFlag[i] = null;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)&&NextTurn){
			LastSpike.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,20),ForceMode2D.Impulse);
			if(currplayer>=playersC){
				currplayer=0;
				for(int i=1;i<playersC;i++){
					if(PlayersFlag[i-1]==null)
						currplayer++;
					else{
						currplayer++;
						break;
					}
				}
			}
			else{
				bool exit=false;
				Debug.Log(currplayer);
				for(int i=currplayer;i<playersC;i++){
					if(PlayersFlag[i]==null){
						if(i>=playersC){//se siamo alla fine ritorna dal player 1
							i=1;
							currplayer=0;
						}
						else{
							currplayer++;
						}
						Debug.Log("da");
					}
					else{
						currplayer++;
						exit=true;
						break;
					}
				}
				if(!exit)
					currplayer=1;
			}
			ActivePlayerFlag(currplayer>0?currplayer:1);
		}
		if(ResetView){
			Time.timeScale=1;
			gameObject.transform.position = Vector3.Lerp(gameObject.transform.position,new Vector3(0,0,-10),10*Time.deltaTime);
			gameObject.GetComponent<Camera>().orthographicSize = Mathf.Lerp(gameObject.GetComponent<Camera>().orthographicSize,5,10*Time.deltaTime);
			gameObject.GetComponent<Camera>().backgroundColor = Color.Lerp(gameObject.GetComponent<Camera>().backgroundColor,Color.white,5*Time.deltaTime);
		
			Invoke("Reset",0.4f);
		}
	}
	public void CurrPlayerEliminated(){
		PlayersFlag[currplayer-1]=null;
		int id=0;
		foreach(GameObject go in PlayersFlag)
			if(go)
				id++;
		if(id==1)
			Invoke("EndGame",2);
	}
	public void EndGame(){
		Destroy(GameObject.Find("GameInf"));
		Application.LoadLevel(0);
	}
	void Reset(){
		NextTurn=true;
		InstantiateSpike();
		ResetView=false;
		gameObject.transform.position = new Vector3(0,0,-10);
		gameObject.GetComponent<Camera>().backgroundColor = Color.white;
		gameObject.GetComponent<Camera>().orthographicSize = 5;
		CancelInvoke("Reset");
	}
	void ActivePlayerFlag(int i){
		Debug.Log(i);
		foreach(GameObject go in PlayersFlag)
			if(go)
			go.SetActive(false);
		PlayersFlag[i-1].SetActive(true);
		NextTurn=false;
	}
	public void InstantiateSpike(){
		LastSpike = (GameObject)Instantiate(Source_Spike);
	}
}
